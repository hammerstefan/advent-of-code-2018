#include <iostream>
#include <fstream>

using Unit = char;
using Polymer = std::string;

Polymer loadData() {
    std::ifstream inFile("input");
    Polymer retval;
    inFile >> retval;
    return retval;
}

Unit swapPolarity(Unit unit) {
    constexpr char diff = 'a' - 'A';
    return unit >= 'a' ? (unit - diff) : (unit + diff);
}


Polymer reactPolymer(Polymer polymer) {
    for (auto it = polymer.begin(); it != polymer.end();) {
        auto next = std::next(it);
        if (next != polymer.end() && (*next == swapPolarity(*it) )) {
            auto old = polymer;
            it = std::prev(polymer.erase(it, std::next(next)));
        }
        else {
            ++it;
        }
    }
    return polymer;
}

int main() {
    Polymer polymer = loadData();
    std::cout << "Size original: " << polymer.size() << '\n';
    Polymer reduced = reactPolymer(polymer);
    std::cout << "Size reduced : " << reduced.size() << '\n';
    return 0;
}