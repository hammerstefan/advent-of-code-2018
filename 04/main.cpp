#include <iostream>
#include <vector>
#include <fstream>
#include <regex>
#include <map>
#include <unordered_map>
#include <iomanip>
#include <numeric>
#include "range/v3/core.hpp"
#include "range/v3/view/transform.hpp"
#include "range/v3/view/filter.hpp"
#include "range/v3/algorithm/count.hpp"
#include "range/v3/numeric/accumulate.hpp"
#include "range/v3/algorithm/max_element.hpp"

using Date = std::string;
using Minute = int;
using GuardId = int;
struct Data {
    Date date;
    Minute minute;
    std::string text;
};

struct DataForTime {
    Minute minute;
    GuardId guardId;
    std::string text;
};
using DataByDate = std::map<Date, std::vector<DataForTime>>;

struct Record {
    GuardId guardId;
    std::array<bool, 60> sleep;
};
using RecordByDate = std::map<Date, Record>;

auto maxByMapData = [] (auto const& i1, auto const& i2) {
    return i1.second < i2.second;
};

Data parse(const std::string& line) {
//    [1518-02-06 00:24] falls asleep
    const std::regex re(R"regex(\[\d+-(.*) \d+:(.*)] (.*))regex");
    std::smatch match;
    if (std::regex_match(line, match, re)){
        return Data {
            match[1],
            std::stoi(match[2]),
            match[3]
        };
    }
    std::cerr << "Failed to match: " << line << '\n';
    return Data();
}

std::vector<Data> getData() {
    std::vector<Data> retval;
    std::ifstream file;
    file.open("input.sorted");
    std::string line;
    while (std::getline(file, line)) {
        retval.push_back(parse(line));
    }
    return retval;
}

bool isGuardChange(const Data& data) {
    std::regex re(R"regex(Guard #(\d+) begins shift)regex");
    std::smatch match;
    return std::regex_match(data.text, match, re);
}

int getNewGuard(const Data& data) {
    std::regex re(R"regex(Guard #(\d+) begins shift)regex");
    std::smatch match;
    if (std::regex_match(data.text, match, re))
        return std::stoi(match[1]);
    std::cerr << "getNewGuard() is not new guard" << '\n';
    return 0;
}


DataByDate groupByDate(const std::vector<Data>& data) {
    DataByDate retval;
    int currentGuardId = 0;
    std::string currentDate;
    std::vector<DataForTime> dataForDate;
    for (const Data& item : data) {
        if (isGuardChange(item)) {
            currentGuardId = getNewGuard(item);
        }
        else {
            if (item.date != currentDate) {
                if (!dataForDate.empty()) {
                    retval.insert({currentDate, dataForDate});
                    dataForDate.clear();
                    currentDate = item.date;
                }
            }
            dataForDate.push_back(DataForTime{
                    item.minute,
                    currentGuardId,
                    item.text
            });
        }
    }
    return retval;
}

std::array<bool, 60> getSleepArray (const std::unordered_map<Minute, bool>& sleepMap) {
    std::array<bool, 60> retval;
    bool isAsleep = false;
    for (int i = 0; i < 60; i++) {
        if (sleepMap.find(i) != sleepMap.end()) {
            isAsleep = sleepMap.at(i);
        }
        retval[i] = isAsleep;
    }
    return retval;
}

Record getRecord(const std::vector<DataForTime>& data) {
    using namespace ranges;
    Record record;
    if (data.empty()) return record;
    record.guardId = data[0].guardId;
    const auto sleepMap = data | view::transform(
            [](const DataForTime& d) {
                return std::make_pair(
                        d.minute,
                        d.text == "falls asleep"
                );
            });
    record.sleep = getSleepArray(sleepMap);
    return record;
}

RecordByDate getRecordByDate(const DataByDate& data) {
    RecordByDate retval;
    for (const auto& item : data) {
        Record record = getRecord(item.second);
        retval.insert({item.first, record});
    }
    return retval;
}

std::unordered_map<GuardId, int> getGuardSleepTimes(const RecordByDate &data) {
    std::unordered_map<GuardId, int> guardSleepTimes;
    for (const RecordByDate::value_type& day : data) {
        Record record = day.second;
        const int sleepHours = ranges::count(record.sleep, true);
        guardSleepTimes[record.guardId] += sleepHours;
    }
    return guardSleepTimes;
}

Minute getMostFrequentSleepMinute(const GuardId guardId, const RecordByDate& recordByDate) {
    RecordByDate recordsForGuard;
    const std::vector<std::array<bool, 60>> sleepEntries = recordByDate
        | ranges::view::filter(
            [guardId](const auto& item) {
                return item.second.guardId == guardId;
            } )
        | ranges::view::transform(
            [](const auto& item) {
                return item.second.sleep;
            }
    );

    std::map<Minute, int> freqByMinute;
    for (Minute minute = 0; minute < 60; minute++) {
        freqByMinute[minute] = ranges::accumulate(
                sleepEntries,
                0,
                [minute] (int total, const auto& item) {
                    return total + item[minute];
                }
        );
    }
    return ranges::max_element(
            freqByMinute,
            maxByMapData
    )->first;
}


void part1(const RecordByDate &recordByDate, const std::unordered_map<GuardId, int> &sleepTime) {
    const GuardId maxGuard = ranges::max_element(
            sleepTime,
            maxByMapData
    )->first;
    std::cout << "Guard who slept most: " << maxGuard << '\n';

    const Minute mostFrequentMintute = getMostFrequentSleepMinute(maxGuard, recordByDate);
    std::cout << "Most slept mninute: " << mostFrequentMintute << '\n';
    std::cout << "GuardId * Minute: " << maxGuard * mostFrequentMintute << '\n';
}

int main() {
    std::vector<Data> data = getData();
    DataByDate dataByDate = groupByDate(data);
    RecordByDate recordByDate = getRecordByDate(dataByDate);
    std::unordered_map<GuardId, int> sleepTime = getGuardSleepTimes(recordByDate);
//    for (const auto& item : recordByDate) {
//        std::cout << item.first << ' ' << std::setw(4) << item.second.guardId << ' ';
//        for (const bool d : item.second.sleep) {
//         std::cout << d;
//        }
//        std::cout << '\n';
//    }

    part1(recordByDate, sleepTime);

    // PART2
    // which guard is most frequently asleep on the same minute?
    std::unordered_map<GuardId, std::unordered_map<Minute, int>> guardSleepCount;
    for (const RecordByDate::value_type& day : recordByDate) {
        Record record = day.second;
        for (Minute min = 0; min < 60; min++) {
            guardSleepCount[record.guardId][min] += record.sleep[min];
        }
    }

    std::unordered_map<GuardId, int> guardMaxCount = ranges::view::transform(
            guardSleepCount,
            [] (const auto& item) {
                const auto& [guardId, sleepCounts] = item;
                return std::make_pair(
                        guardId,
                        ranges::max_element(
                                sleepCounts,
                                maxByMapData
                        )->second
                );
            }
    ) | ranges::max_element(maxByMapData)
//    ranges::max_element(
//            [](const auto& i1, const auto& i2) {
//
//            })

    return 0;
}
