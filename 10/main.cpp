#include <iostream>
#include <vector>
#include <fstream>
#include <regex>
#include <chrono>
#include "fmt/ostream.h"
#include "fmt/core.h"
#include "range/v3/action/push_back.hpp"
#include "range/v3/numeric/accumulate.hpp"
#include "range/v3/view/drop.hpp"
#include "range/v3/view/for_each.hpp"
#include "range/v3/view/iota.hpp"
#include "range/v3/view/join.hpp"
#include "range/v3/view/repeat_n.hpp"
#include "range/v3/view/sample.hpp"
#include "range/v3/view/slice.hpp"
#include "range/v3/view/single.hpp"
#include "range/v3/view/take.hpp"
#include "range/v3/view/transform.hpp"
#include "range/v3/view/zip.hpp"
#include "range/v3/view/zip_with.hpp"
#include "range/v3/algorithm/for_each.hpp"
#include "range/v3/core.hpp"
#include "Vector2.h"
#include "PointData.h"

using namespace ranges;

auto printWithCr = [](const auto& d) {
    fmt::print("{}\n", d);
};

std::vector<PointData> readData(const std::string& filepath);
PointData parseLine(const std::string& line);
void tick(PointData& data);

template<typename F, typename S>
std::ostream& operator<< (std::ostream& os, ranges::common_pair<F, S> p) {
    os << fmt::format("[{},{}]", p.first, p.second);
    return os;
}

namespace fmt {
    template <typename F, typename S>
    struct formatter<common_pair<F, S>> {
        template <typename ParseContext>
        constexpr auto parse(ParseContext &ctx) { return ctx.begin(); }

        template <typename FormatContext>
        auto format(const std::pair<int, int> &p, FormatContext &ctx) {
            return format_to(ctx.begin(), "[{},{}]", p.first, p.second);
        }
    };
}

template <typename T>
auto combinations(const T& data) {
    const int len = ranges::distance(data);

    return view::for_each(view::ints(0, len), [&data, len](int i) {
        auto list = view::drop(data, i);
        return view::zip(
                view::repeat_n(front(list), len-i),
                view::drop(list, 1));
    });
}

template<typename T>
double findAverageDistance(const T& positions, double precision) {
    const int sampleSize = distance(positions) * precision;
    return ranges::accumulate(
        combinations(view::take(positions,sampleSize) | to_vector) | view::transform([](const auto& p) {
            return std::sqrt(
                    std::pow(p.first.pos.x - p.second.pos.x, 2)) +
                   std::pow(p.first.pos.y - p.second.pos.y, 2);
        }),
        0.0
    ) / ranges::distance(positions);
}

std::vector<PointData> readData(const std::string& filepath) {
    std::ifstream file (filepath);
    std::string line;
    std::vector<PointData> data;
    while (std::getline(file, line)) {
        data.push_back(parseLine(line));
    }
    return data;
}

PointData parseLine(const std::string& line) {
    std::regex re {R"regex(position=< *([-0-9]+), *([-0-9]+)> velocity=< *([-0-9]+), *([-0-9]+)>)regex"};
    std::smatch match;
    if (std::regex_match(line, match, re)) {
        const int pos_x = std::stoi(match[1]);
        const int pos_y = std::stoi(match[2]);
        const int vel_x = std::stoi(match[3]);
        const int vel_y = std::stoi(match[4]);
        return PointData {
            Vector2 {pos_x, pos_y},
            Vector2 {vel_x, vel_y}
        };
    }
    else {
        fmt::print("Unable to re match line '{}'", line);
        return PointData{};
    }
}

auto positions(const std::vector<PointData>& data) {
    return view::transform(data, [](const PointData& d) {
        return d.pos;
    });
}

void tick(std::vector<PointData>& data) {
    auto tickPointData = [](PointData& d) {d.pos += d.velocity;};
    for_each(data, tickPointData);
}

void toCsv(const std::vector<PointData>& data, const std::string& fileName) {
    std::ofstream file(fileName);
    for_each(data, [&file](const PointData& d) {
        fmt::print(file, "{},{}\n", d.pos.x, d.pos.y);
    });
}

int main() {
    std::vector<PointData> data = readData("input.large");
    tick(data);
    double prevAvgDistance = findAverageDistance(data, 1.0);
    double avgDistance = prevAvgDistance;
    std::vector<PointData> prevData;
    auto t_initial = std::chrono::system_clock::now();
    while (avgDistance <= prevAvgDistance) {
        auto t1 = std::chrono::system_clock::now();
        prevData = data;
        tick(data);
        prevAvgDistance = avgDistance;
        avgDistance = findAverageDistance(data, 0.2);
        auto t2 = std::chrono::system_clock::now();
        fmt::print("Average Distance: {} in {}ms\n",
                   avgDistance,
                   std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count());
    }
    auto t_final= std::chrono::system_clock::now();
    fmt::print("Total time: {}ms", std::chrono::duration_cast<std::chrono::milliseconds>(t_final-t_initial).count());
    for_each(positions(prevData), printWithCr);
    toCsv(prevData, "final.csv");
//    double averageDistance = findAverageDistance(positions(data));
    return 0;
}