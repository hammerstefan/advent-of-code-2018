//
// Created by shammer on 12/21/18.
//

#include "PointData.h"
#include "fmt/core.h"
#include "fmt/ostream.h"

std::ostream& operator<< (std::ostream& os, const PointData& vector) {
    os << fmt::format(
            "{} :  {}",
            vector.pos,
            vector.velocity
    );
    return os;
}
