//
// Created by shammer on 12/21/18.
//

#ifndef INC_10_VECTOR2_H
#define INC_10_VECTOR2_H

#include <ostream>

struct Vector2 {
    int x;
    int y;
    Vector2& operator+= (const Vector2& v);
    Vector2 operator+ (const Vector2& v) const;
};

std::ostream& operator<< (std::ostream& os, const Vector2& vector);

#endif //INC_10_VECTOR2_H
