//
// Created by shammer on 12/21/18.
//

#include "Vector2.h"
#include "fmt/core.h"
#include "fmt/ostream.h"

Vector2& Vector2::operator+= (const Vector2& v) {
    x += v.x;
    y += v.y;
    return *this;
}
Vector2 Vector2::operator+ (const Vector2& v) const {
    Vector2 result = *this;
    result += v;
    return result;
}

std::ostream &operator<<(std::ostream &os, const Vector2 &vector) {
    os << fmt::format(
            "<{:4},{:4}>",
            vector.x,
            vector.y
    );
    return os;
}
