//
// Created by shammer on 12/21/18.
//

#ifndef INC_10_POINTDATA_H
#define INC_10_POINTDATA_H

#include "Vector2.h"

struct PointData {
    Vector2 pos;
    Vector2 velocity;
};

std::ostream& operator<< (std::ostream& os, const PointData& vector);

#endif //INC_10_POINTDATA_H
