#include <iostream>
#include <vector>
#include <fstream>
#include <regex>
#include <unordered_map>
#include <numeric>
#include <utility>


using XPos = int;
using YPos = int;
struct Point {
    int x;
    int y;
};

struct Rect {
    int topLeftX;
    int topLeftY;
    int width;
    int height;
    std::string toString() const {
        std::stringstream str;
        str << '@' << topLeftX << ',' << topLeftY << ' ' << width << 'x' << height;
        return str.str();
    }
    std::vector<Point> points() const {
        std::vector<Point> retval;
        for (int x = topLeftX; x < topLeftX + width; x++) {
            for (int y = topLeftY; y < topLeftY + height; y++) {
                retval.push_back({x, y});
            }
        }
        return retval;
    }
};

struct Data {
    int id;
    Rect rect;
};

void printData(const Data& data) {
    std::cout << "id: " << data.id << ' ' << data.rect.toString() << '\n';
}


Data parse(const std::string &basic_string);

std::vector<Data> getData() {
    std::fstream inFile;
    inFile.open("input.txt");
    std::string line;
    std::vector<Data> retval;
    while (std::getline(inFile, line)) {
        const Data data = parse(line);
        retval.push_back(data);
    }
    return retval;
}

Data parse(const std::string& val) {
//    #1290 @ 72,249: 18x15
    const std::regex re("^#(.*) @ (.*),(.*): (.*)x(.*)$");
    std::smatch match;
    if (std::regex_match(val, match, re)) {
        const int id = std::stoi(match[1]);
        const int topLeftX = std::stoi(match[2]);
        const int topLeftY = std::stoi(match[3]);
        const int width = std::stoi(match[4]);
        const int height = std::stoi(match[5]);
        return Data{
                id,
                Rect{topLeftX,
                     topLeftY,
                     width,
                     height
                }
        };
    }
    std::cout << "Failed to parse: " << val << '\n';
    return Data();
}

bool operator== (const Point& one, const Point& two) {
    return one.x == two.x && one.y == two.y;
}

struct PointHash {
    std::size_t operator() (const Point& p) const {
        return p.x ^ (p.y << 8);
    }
};

using LayoutMap = std::unordered_map<Point, int, PointHash>;

void printLayout(const LayoutMap& layout) {
    auto maxX = [] (const auto& item1, const auto& item2) {
        return item1.x < item2.y;
    };
    auto maxY = [] (const auto& item1, const auto& item2) {
        return item1.y < item2.y;
    };

    std::vector<Point> pos;
    std::transform(
            layout.begin(),
            layout.end(),
            std::back_inserter(pos),
            [](const auto& item) {
                return item.first;
            }
    );
    const XPos maxXVal = std::max_element(pos.begin(), pos.end(), maxX)->x;
    const YPos maxYVal = std::max_element(pos.begin(), pos.end(), maxY)->y;

    for (const auto& item : layout) {
        std::cout << '(' << item.first.x << ',' << item.first.y << "): " << item.second << '\n';
    }
}

void addOccupancy(LayoutMap& layout, const Rect& rect) {
    for (const Point& point : rect.points()) {
        if (layout.find(point) == layout.end())
            layout.insert({point, 1});
        else
            layout.at(point)++;
    }
}

LayoutMap findOverlap(const std::vector<Data> & data) {
    LayoutMap layout;

    std::for_each(
            data.begin(),
            data.end(),
            [&layout](const auto& datum) {addOccupancy(layout, datum.rect);}
    );

    const int overlapped = (int)std::count_if(
            layout.begin(), layout.end(),
            [] (const auto& item) { return item.second > 1;}
    );

    std::cout << "Overlapped squared: " << overlapped << '\n';
    return layout;
}

bool checkRectForOverlap(const Rect& rect, const LayoutMap& layout) {
    for (const Point& point : rect.points()) {
        if (layout.at(point) > 1)
            return true;
    }
    return false;
}

void findGoodSquare(const std::vector<Data>& data, const LayoutMap& layout) {
    for (const Data& item: data) {
        if (!checkRectForOverlap(item.rect, layout)) {
            std::cout << "Perfect rect : #" << item.id << ' ' << item.rect.toString() << '\n';
        }
    }
}

int main() {
    std::vector<Data> values = getData();
    auto layout = findOverlap(values);
    findGoodSquare(values, layout);
    return 0;
}