#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <set>


std::vector<std::string> getValues() {
    std::fstream inFile;
    inFile.open("input.txt");
    std::string val;
    std::vector<std::string> retval;
    while (inFile >> val)
        retval.push_back(val);
    return retval;
}

std::tuple<int, int> getCounts(const std::string & val) {
   int doubles = 0, triples = 0;
   std::unordered_map<char, int> chars;
   for (char c : val) {
       chars[c]++;
   }

   for (auto pair : chars) {
       if (pair.second == 2) {
           doubles = 1;
       }
       else if (pair.second == 3) {
           triples = 1;
       }
   }

   return std::make_tuple(doubles, triples);
}

int checksum(const std::vector<std::string> & values) {
    int doublesCount = 0, triplesCount = 0;
    for (auto val : values) {
        auto [doubles, triples] = getCounts(val);
        doublesCount += doubles;
        triplesCount += triples;
    }
    int checksum = doublesCount * triplesCount;
    std::cout << "Checksum: " << checksum << '\n';
}

bool stringsDifferOneChar(const std::string & val1, const std::string & val2) {
    int diff = 0;
    for (int i = 0; i < val1.size(); i++) {
        if (val1[i] != val2[i]) {
            diff++;
        }
    }
    return diff == 1;
}

std::string commonChars(const std::string& val1, const std::string& val2) {
    std::string common;
    for (int i = 0; i < val1.size(); i++) {
        if (val1[i] == val2[i]) {
            common.push_back(val1[i]);
        }
    }
    return common;
}

int main() {
    auto values = getValues();
    checksum(values);

    std::string matchedVal;
    for (const auto & val1 : values) {
        for (const auto & val2 : values) {
            if (val1 != val2 && stringsDifferOneChar(val1, val2)) {
                matchedVal = commonChars(val1, val2);
            }
        }
    }
    std::cout << "Common chars: " << matchedVal << '\n';

    return 0;
}