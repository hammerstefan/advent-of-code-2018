#include <iostream>
#include <vector>
#include <fstream>

using CharMatrix = std::vector<std::vector<char>>;
using Coord = std::pair<size_t, size_t>;

enum class Turn {
    LEFT,
    STRAIGHT,
    RIGHT
};

struct Cart {
    Coord pos;
    Turn nextTurn = Turn::LEFT;
};

CharMatrix getInput(std::string fileName) {
    CharMatrix retVal;
    std::ifstream file(fileName);

    std::string line;
    while (std::getline(file, line)) {
        std::vector<char> lineArray;
        std::copy(begin(line), end(line), std::back_inserter(lineArray));
        retVal.push_back(lineArray);
    }
    return retVal;
}

std::vector<Cart> findCarts(const CharMatrix& map) {
    const size_t height = map.size();
    const size_t width = map.size() ? map[0].size() : 0;

    auto isCart = [] (const char c) {
        return std::string("<^>v").find(c) != std::string::npos;
    };

    std::vector<Cart> retval;
    for (size_t y = 0; y < height; y++) {
        for (size_t x = 0; x < width; x++) {
            if (isCart(map[y][x])) {
                retval.push_back({Coord{x, y}, Turn::LEFT});
            }
        }
    }
    return retval;
}

std::ostream& operator<<(std::ostream& os, const std::vector<Coord>& list) {
    for (auto c : list) {
        os << c.first << ',' << c.second << '\n';
    }
    return os;
}

Coord findNextCoord(const Coord& coord, char cartSymbol) {
    switch (cartSymbol) {
        case '<':
            return Coord {coord.first - 1, coord.second};
        case '>':
            return Coord {coord.first + 1, coord.second};
        case '^':
            return Coord {coord.first, coord.second - 1};
        case 'v':
            return Coord {coord.first, coord.second + 1};
        default:
            return Coord{};
    }
}

char findNextSymbol(const char location, const char currentSymbol, Cart& cart) {
    switch (location) {
        case '/':
            switch (currentSymbol) {
                case '^': return '>';
                case '<': return 'v';
                case '>': return '^';
                case 'v': return '<';
            }
            break;
        case '\\':
            switch (currentSymbol) {
                case '^': return '<';
                case '<': return '^';
                case '>': return 'v';
                case 'v': return '>';
            }
            break;
        case '+':
            // make turn decision based off of cart.nextTurn;

    }
}

std::vector<Cart> tick(CharMatrix& map, const std::vector<Cart>& carts) {
    CharMatrix originalMap = map;
    std::vector<Coord> newCarts;
    for (auto& cart : carts) {
        const Coord& pos = cart.pos;
        const char cartSymbol = map[pos.second][pos.first];
        Coord nextCoord = findNextCoord(pos, cartSymbol);
        char nextSymbol = findNextSymbol(originalMap[nextCoord.second][nextCoord.first], cartSymbol, cart);
    }
    return carts;
}

int main() {
    CharMatrix map = getInput("input.txt");
    std::vector<Cart> carts = findCarts(map);
    std::cout << carts;
    carts = tick(map, carts);
    return 0;
}