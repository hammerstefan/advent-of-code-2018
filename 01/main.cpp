#include <iostream>
#include <fstream>
#include <istream>
#include <string>
#include <unordered_map>
#include <vector>

void findSum() {
    std::ifstream inFile;
    inFile.open("input.txt");
    int val;
    int sum = 0;
    while (inFile >> val) {
        sum += val;
    }
    std::cout << "Sum: " << sum << '\n';
}

std::vector<int> getValues() {
    std::ifstream inFile;
    inFile.open("input.txt");
    std::vector<int> retval;
    int val;
    while (inFile >> val) {
        retval.push_back(val);
    }
    return retval;
}

void findDup() {
    auto values = getValues();
    int sum = 0;
    std::unordered_map<int, int> freqs;
    freqs[sum]++;
    int firstDup = 0;
    bool foundFirstDup = false;
    auto item = values.begin();
    size_t iterations = 0;
    while (!foundFirstDup) {
        iterations++;
        sum += *item;
        freqs[sum]++;
        if (++item == values.end())
            item = values.begin();
        if (!foundFirstDup && freqs[sum] == 2) {
            firstDup = sum;
            foundFirstDup = true;
        }
    }
    std::cout << "First dup: " << firstDup << '\n';
    std::cout << "Iterations: " << iterations << '\n';

}

int main() {
//    findSum();
    findDup();
    return 0;
}